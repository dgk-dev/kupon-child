(function ($) {
    $(document).ready(function () {
        console.log('checking location....');
        if (!"geolocation" in navigator) {
            console.log('Location on');
            navigator.geolocation.getCurrentPosition(function (position) {
                localStorage.setItem('ep_lat', position.coords.latitude);
                localStorage.setItem('ep_lon', position.coords.longitude);
            });
        }else{
            console.log('Location off');
        }
    });
})(jQuery);