<?php
get_header();
$options = get_option(AZEXO_FRAMEWORK);
?>

<div class="<?php print ((isset($options['content_fullwidth']) && $options['content_fullwidth']) ? '' : 'container'); ?>">
    <div id="primary" class="content-area">
        <div id="content" class="site-content" role="main">
            <div class="page-wrapper">
                <div class="page-content">
                    <p style="font-size: 5em; font-weight: bold; margin-bottom: 1em;"><span style="font-size: 0.8em">error</span> 404</p>
                    <h3>Parece que la página que intentas encontrar no existe.</h3>
                    <p>Intenta realizar una búsqueda o visita <a href="<?php echo get_permalink( wc_get_page_id( 'shop' ) ); ?>">nuestra tienda</a> para encontrar los mejores descuentos.</p>
                </div><!-- .page-content -->
            </div><!-- .page-wrapper -->

        </div><!-- #content -->
    </div><!-- #primary -->
</div>
<?php get_footer(); ?>