(function ($) {

    $.fn.multipleInput = function(){

        return this.each(function(index){
            $this = $(this);
            name = $this.attr('name') ? $this.attr('name') : '';
            valueIndex = 0;
            values = {};
            $list = $('<ul />');
            $hidden = $('<input type="hidden" name="' + name + '" />');

            prevValues = $this.val().split(',');

            $.each(prevValues, function(index, value){
                if(value) setEntry(value);
            });

            // input
            var $input = $('<input class="multiple-input-entry" type="text" placeholder="Agregar elemento" />').keyup(function(e){
                e.preventDefault();
                var keyCode = e.keyCode || e.which;
                var $this = $(this);
                if (keyCode == 13) {
                    var val = $this.val(); // remove space/comma from value
                    setEntry(val);
                    $this.val('');
                }

            });

            function setEntry(val){
                // append to list of elements with remove button
                $list.append($('<li class="multipleInput-entry"><span>' + val + '</span></li>')
                    .append($('<a href="#" class="multipleInput-close" data-value-index="' + valueIndex + '" title="Remove" />')
                        .click(function (e) {
                            e.preventDefault();
                            var $this = $(this);
                            delete values[$this.data('value-index')];
                            $this.parent().remove();
                            setHiddenValue();
                        })
                    )
                );
                values[valueIndex] = val;
                valueIndex++;
                setHiddenValue();
            }

            function setHiddenValue(){
                $hidden.val(Object.values(values).join(','));
            }

            // container div
            var $container = $('<div class="multipleInput-container" />').click(function(){
                $input.focus();
            });

            // insert elements into DOM
            $container.append($list).append($input).append($hidden).insertAfter($(this));

            return $(this).hide().attr('name', '');

        });

    };

    $('.multiple-input').multipleInput();
    $('.multiple-input-entry').on('keyup keypress', function(e){
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) {
            e.preventDefault();
            return false;
        }
    });
})(jQuery);