(function($){
    var $analyticsCode = $('#analytics_code');
    if($analyticsCode.length){
        var $eventCheck = $('#analytics_add_view_product_event');
        
        set_add_event_check();
        
        $analyticsCode.live("keyup", function(e){
            set_add_event_check();
        });
    
        function set_add_event_check(){
            if(!$analyticsCode.val() || $analyticsCode.val()=='<empty string>'){
                $eventCheck.removeAttr('checked');
                $eventCheck.attr('disabled', 'disabled');
            }else{
                $eventCheck.removeAttr('disabled');
            }
        }
    }
    
})(jQuery)