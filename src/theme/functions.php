<?php

/** analytics */
// require(get_stylesheet_directory().'/includes/analytics.php');

/** Cambios en tienda */
require(get_stylesheet_directory().'/includes/store-changes.php');

/** Mapa para elegir marcador en productos */
// require(get_stylesheet_directory().'/includes/products-map.php');

add_action('wp_enqueue_scripts', 'ep_resources', 15);
function ep_resources(){
    wp_enqueue_style( 'azexo-style', get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'azexo-child-style', get_stylesheet_directory_uri() . '/style.css', array( 'azexo-skin', 'azexo-style' ), wp_get_theme()->get('Version') );
    wp_enqueue_script('footer_js', get_stylesheet_directory_uri() . '/js/footer-bundle.js', array('jquery'), 1.0, true);
	// wp_localize_script('footer_js', 'footerGlobalObject', array(
	// 	'ajax_url' => admin_url('admin-ajax.php')
    // ));
    wp_dequeue_script('jquery-sticky-kit');
    
}

//langs: themes and plugins
add_action( 'after_setup_theme', 'ep_theme_lang' );
function ep_theme_lang() {
    load_child_theme_textdomain( 'AZEXO', get_stylesheet_directory() . '/language' );

    //plugins
    unload_textdomain('fslm');
    load_textdomain('fslm', get_stylesheet_directory() . '/language/fslm-es_ES.mo');
}

/**
 * Reescribe etiquetas
 */
add_filter( 'gettext', 'ep_change_translation_strings', 20, 3 );
function ep_change_translation_strings( $translated_text, $text, $domain ) {
    switch ( $translated_text ) {
        case 'Maps and Directions' :
            $translated_text = __( 'Mapa', $domain );
        break;
        case 'Search' :
            $translated_text = __( 'Buscar', $domain );
        break;
        case 'Guardar mi nombre, correo electrónico y web en este navegador para la próxima vez que comente.' :
            $translated_text = __( 'Guardar mi nombre y correo electrónico en este navegador para la próxima vez que comente.', $domain );
        break;
        case 'Your comment is awaiting approval' :
            $translated_text = __( 'Tu valoración está pendiente de aprobación', $domain );
        break;
    }
    return $translated_text;
}

/**
 * Proper ob_end_flush() for all levels
 *
 * This replaces the WordPress `wp_ob_end_flush_all()` function
 * with a replacement that doesn't cause PHP notices.
 */
// remove_action( 'shutdown', 'wp_ob_end_flush_all', 1 );
// add_action( 'shutdown', function() {
//    while ( @ob_end_flush() );
// } );