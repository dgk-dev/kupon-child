<?php

// Add assets
function ep_products_map_assets() {
    $screen = get_current_screen();
    if($screen->id == 'product'){
        $gmap_api_key = 'AIzaSyBxfncg-b5W3Sd9VdRCvtmS3VeQcDAb05g';
        if (function_exists('cmb2_get_option')) {
            $gmap_api_key = cmb2_get_option('azqf_options', 'gmap_api_key') ? cmb2_get_option('azqf_options', 'gmap_api_key') : $gmap_api_key;
        }
        wp_enqueue_script( 'google-maps-native', 'http://maps.googleapis.com/maps/api/js?key='.$gmap_api_key.'&libraries=places');
        wp_enqueue_script( 'map-script', get_stylesheet_directory_uri().'/admin/js/map.js');
    }
}
add_action( 'admin_enqueue_scripts', 'ep_products_map_assets', 50 );

// Create Metabox
function ep_products_map_add_embed_gmaps_meta_box() {
    add_meta_box(
        'gmaps_embed_meta_box', // $id
        'Ubicación', // $title
        'ep_products_map_show_embed_gmaps_meta_box', // $callback
        'product', // $page
        'normal', // $context
        'high'); // $priority
}
add_action('add_meta_boxes', 'ep_products_map_add_embed_gmaps_meta_box');

// Show Metabox Contents
function ep_products_map_show_embed_gmaps_meta_box() {
    global $post;  
	$lat = get_post_meta($post->ID, 'latitude', true);  
	$lng = get_post_meta($post->ID, 'longitude', true); 	
	$nonce = wp_create_nonce(basename(__FILE__));
?>
<style>
#map-canvas {
	height: 50vh;
	width: 100%;
}
#ep-map-data input{
    width: 100%;
}
#ep-map-data textarea{
    width: 100%;
}

#map-search input, #map-search button{
    width: 100%;
}
</style>
<table style="width: 100%;">
    <tbody class="form-table" id="map-search">
        <tr>
            <th style="font-weight:normal">
                <input id="map-search-input" type="text" placeholder="Buscar en el mapa" />
            </th>
            <td style="width:20%; text-align: center">
                <button class="button button-primary" id="map-search-button">Buscar</button>
            </td>
        </tr>
    </tbody>
</table>

<div class="maparea" id="map-canvas"></div>
<input type="hidden" name="ep_map_meta_box_nonce" value="<?php echo $nonce; ?>">
<table style="width: 100%;">
    <tbody class="form-table" id="ep-map-data">
        <tr>
            <th style="font-weight:normal">
                <label for="ep-latitude"><?php _e( 'Latitud', 'AZEXO' )?></label>
            </th>
            <td>
                <input class="location-input" name="latitude" type="text" id="ep-latitude" value="<?php echo $lat; ?>" />
            </td>
        </tr>
        <tr>
            <th style="font-weight:normal">
                <label for="ep-longitude"><?php _e( 'Longitud', 'AZEXO' )?></label>
            </th>
            <td>
                <input class="location-input" name="longitude" type="text" id="ep-longitude" value="<?php echo $lng; ?>" />
            </td>
        </tr>
    </tbody>
</table>
<?php
}

// Save Metaboxes.
function ep_products_map_save_embed_gmap($post_id) {   
    // verify nonce
    if (!wp_verify_nonce($_POST['ep_map_meta_box_nonce'], basename(__FILE__)))
        return $post_id;
        
    // check autosave
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
        return $post_id;
        
    // check permissions
    if ('page' == $_POST['post_type']) {
        if (!current_user_can('edit_page', $post_id))
            return $post_id;
        } elseif (!current_user_can('edit_post', $post_id)) {
            return $post_id;
    }  
    
    $oldlat = get_post_meta($post_id, "latitude", true);
    $newlat = $_POST["latitude"]; 
    if ($newlat != $oldlat) {
        update_post_meta($post_id, "latitude", $newlat);
    } 

    $oldlng = get_post_meta($post_id, "longitude", true);
    $newlng = $_POST["longitude"]; 
    if ($newlng != $oldlng) {
        update_post_meta($post_id, "longitude", $newlng);
    }
}
add_action('save_post', 'ep_products_map_save_embed_gmap');