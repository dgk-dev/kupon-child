<?php
// //menú líneas de producto
// add_filter('woocommerce_attribute_show_in_nav_menus','ep_register_line_products_menu', 1, 2);
// function ep_register_line_products_menu( $register, $name = '' ) {
//      if ( $name == 'pa_linea-de-producto' ) $register = true;
//      return $register;
// }

// //mensaje extra en recuperar password
// add_action('woocommerce_after_lost_password_confirmation_message', 'ep_after_lost_password_confirmation_message');
// function ep_after_lost_password_confirmation_message(){
// 	echo "<p><strong>Si no lo encuentras en la bandeja de entrada, busca en tu bandeja de spam o correo no deseado.</strong></p>";
// }


/**
 * RFC - Razón social
 */
// Add / Display additional billing fields in checkout and My account > Edit adresses > Billing form
add_filter( 'woocommerce_billing_fields', 'ep_additional_billing_fields', 10, 1 );
function ep_additional_billing_fields( $fields )
{
    $fields['billing_rfc'] = array(
        'type'        => 'text', // add field type
        'label'       => __('RFC', 'woocommerce'), // Add custom field label
        'placeholder' => _x('RFC', 'placeholder', 'woocommerce'), // Add custom field placeholder
        'required'    => false, // if field is required or not
        'clear'       => false, // add clear or not
        'class'       => array( 'form-row-first' ),
    );
    
    $fields['billing_razon_social'] = array(
        'type'        => 'text', // add field type
        'label'       => __('Razón social', 'woocommerce'), // Add custom field label
        'placeholder' => _x('Razón social', 'placeholder', 'woocommerce'), // Add custom field placeholder
        'required'    => false, // if field is required or not
        'clear'       => false, // add clear or not
        'class'       => array( 'form-row-first' ),

    );

    return  $fields;
}

add_filter( 'woocommerce_my_account_my_address_formatted_address', function( $args, $customer_id, $name ){
    if($name == 'billing'){
        $args['rfc'] = get_user_meta( $customer_id, $name . '_rfc', true );
        $args['razon_social'] = get_user_meta( $customer_id, $name . '_razon_social', true );
    }
    return $args;
}, 10, 3 ); 

// modify the address formats
add_filter( 'woocommerce_localisation_address_formats', function( $formats ){
    foreach ( $formats as $key => $format ) {
        // put a break and then the phone after each format.
        $format .= "\n{rfc}";
        $format .= "\n{razon_social}";
    }
    return $formats;
} );

// add the replacement value
add_filter( 'woocommerce_formatted_address_replacements', function( $replacements, $args ){
    // we want to replace {rfc} in the format with the data we populated
    $replacements['{rfc}'] = isset($args['rfc']) ? $args['rfc'] : '';
    $replacements['{razon_social}'] = isset($args['razon_social']) ? $args['razon_social'] : '';
    return $replacements;
}, 10, 2 );

// Get the field values to be displayed in admin Order edit pages
add_filter('woocommerce_order_formatted_billing_address', 'ep_add_woocommerce_order_fields', 10, 2);
function ep_add_woocommerce_order_fields($address, $order ) {
    $address['rfc'] = get_post_meta( $order->get_id(), 'billing_rfc', true );
    $address['razon_social'] = get_post_meta( $order->get_id(), 'billing_razon_social', true );

    return $address;
}

// Make the custom billing field Editable in Admin order pages
add_filter('woocommerce_admin_billing_fields', 'ep_add_woocommerce_admin_billing_fields');
function ep_add_woocommerce_admin_billing_fields($billing_fields) {
    $billing_fields['rfc'] = array( 'label' => __('RFC', 'woocommerce') );
    $billing_fields['razon_social'] = array( 'label' => __('Razón social', 'woocommerce') );

    return $billing_fields;
}

/**Número de referencia en correo */
// add_filter('woocommerce_email_before_order_table', 'ep_email_add_order_reference_number', 10, 4);
// function ep_email_add_order_reference_number($order, $sent_to_admin, $plain_text, $email) {
//     echo '<p style="font-size: 1.25em"><strong>Número de referencia de pedido:</strong> '.$order->get_id().'</p>';
// }

/**Mostrar solo envio gratis si está disponible */
add_filter( 'woocommerce_package_rates', 'ep_hide_shipping_when_free_is_available', 100 );
function ep_hide_shipping_when_free_is_available( $rates ) {
	$free = array();
	foreach ( $rates as $rate_id => $rate ) {
		if ( 'free_shipping' === $rate->method_id ) {
			$free[ $rate_id ] = $rate;
			break;
		}
	}
	return ! empty( $free ) ? $free : $rates;
}

/**Casilla para requerir factura */
add_action( 'woocommerce_after_checkout_billing_form', 'ep_add_send_billing_check' );
function ep_add_send_billing_check( $checkout ) {
    foreach( WC()->cart->get_cart() as $cart_item ) {
        if ( $cart_item['data']->is_virtual() ) return;
     }
    
    woocommerce_form_field( 'send_billing', array(
        'type'	=> 'checkbox',
		'class'	=> array('form-row-wide'),
		'label'	=> '¿Deseas recibir factura?',
    ), $checkout->get_value( 'send_billing' ) );
    
}

add_action( 'woocommerce_checkout_update_order_meta', 'ep_save_send_billing' );
function ep_save_send_billing( $order_id ){ 
	if( !empty( $_POST['send_billing'] ) && $_POST['send_billing'] == 1 )
		update_post_meta( $order_id, 'send_billing', 1 );
}

add_filter( 'woocommerce_email_order_meta_fields', 'ep_woocommerce_email_order_send_billing_field', 10, 3 );
function ep_woocommerce_email_order_send_billing_field( $fields, $sent_to_admin, $order ) {
    $fields['send_billing'] = array(
        'label' => __( '¿Deseas recibir factura?' ),
        'value' => get_post_meta( $order->get_id(), 'send_billing', true ) ? 'Sí' : 'No',
    );
    return $fields;
}

add_action( 'woocommerce_admin_order_data_after_billing_address', 'ep_display_send_billing_on_order_edit_pages', 10, 1 );
function ep_display_send_billing_on_order_edit_pages( $order ){
    $send_billing = get_post_meta( $order->get_id(), 'send_billing', true );
    if( ! empty( $send_billing ) ){
        $billing_text = $send_billing ? 'Sí' : 'No';
        echo '<p><strong>¿Deseas recibir factura?: </strong>'.$billing_text.'</p>';
    }
}


// Display 'pickup html data' in "Order received" and "Order view" pages (frontend)
add_action( 'woocommerce_order_details_after_order_table', 'ep_display_send_billing_data_in_orders', 10 );
function ep_display_send_billing_data_in_orders( $order ) {
    $send_billing = get_post_meta( $order->get_id(), 'send_billing', true );
    if( ! empty( $send_billing ) ){
        $billing_text = $send_billing ? 'Sí' : 'No';
        echo '<p><strong>¿Deseas recibir factura?: </strong>'.$billing_text.'</p>';
    }
}


// /**
//  * Cambiar mensajes de stock
//  */

// add_filter( 'woocommerce_get_availability', 'ep_change_stock_messages', 20, 2 );

// function ep_change_stock_messages( $availability, $_product ) {

//     if ( $_product->is_in_stock() ) {
//         if ( $_product->get_stock_quantity() <  $_product->get_low_stock_amount() ) {
//             $availability['availability'] = __( "Quedan pocas", 'woocommerce' );
//         }else{
//             $availability['availability'] = __( "Hay stock", 'woocommerce' );
//         }
//     }
//     return $availability;
// }

/**
 * Sobreescribir share e producto con estilo del template
 */
add_filter('azexo_entry_field', 'ep_entry_share_filter', 10, 2);
function ep_entry_share_filter($output, $name){
    if($name == 'post_share'){
        $options = get_option(AZEXO_FRAMEWORK);
        ob_start();
        ep_entry_share();
        return '<div class="entry-share">' . '<div class="helper">' . (isset($options['post_share_prefix']) ? esc_html($options['post_share_prefix']) : '') . '</div><span class="links">' . ob_get_clean() . '</span></div>';
            
    }
}

add_action('init', 'ep_override_share', 15);

function ep_override_share(){
    remove_action('woocommerce_share', 'azexo_entry_share');
    add_action('woocommerce_share', 'ep_entry_share');
}

function ep_entry_share(){
    global $post;
    $image = null;
    if (is_object($post)) {
        $image = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
    }
    print '<a class="facebook-share" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=' . rawurlencode(esc_url(apply_filters('the_permalink', get_permalink()))) . '"><span class="share-box"><i class="fa fa-facebook"></i></span></a>';
    print '<a class="twitter-share" target="_blank" href="https://twitter.com/home?status=' . rawurlencode(esc_attr__('Check out this article: ', 'AZEXO')) . rawurlencode(get_the_title()) . '%20-%20' . rawurlencode(esc_url(apply_filters('the_permalink', get_permalink()))) . '"><span class="share-box"><i class="fa fa-twitter"></i></span></a>';
    if (!empty($image)) {
        print '<a class="pinterest-share" target="_blank" href="https://pinterest.com/pin/create/button/?url=' . rawurlencode(esc_url(apply_filters('the_permalink', get_permalink()))) . '&media=' . rawurlencode($image) . '&description=' . rawurlencode(get_the_title()) . '"><span class="share-box"><i class="fa fa-pinterest"></i></span></a>';
    }
    print '<a class="linkedin-share" target="_blank" href="http://www.linkedin.com/shareArticle?mini=true&url=' . rawurlencode(esc_url(apply_filters('the_permalink', get_permalink()))) . '&title=' . rawurlencode(get_the_title()) . '&source=LinkedIn"><span class="share-box"><i class="fa fa-linkedin"></i></span></a>';
    print '<a class="whatsapp-share" target="_blank" href="https://wa.me/?text=' . rawurlencode(esc_url(apply_filters('the_permalink', get_permalink()))) . '&title=' . rawurlencode(get_the_title()) . '"><span class="share-box"><i class="fa fa-whatsapp"></i></span></a>';
    if (comments_open() && !is_single() && !is_page()) {
        $comments = '<span class="share-box"><i class="fa fa-comment-o"></i></span>';
        comments_popup_link($comments, $comments, $comments, '', '');
    }
}

/**
 * Mover valoración de estrellas arriba de formulario
 */
add_filter( 'comment_form_fields', 'codeless_woo_comment_form_fields', 15 );
function codeless_woo_comment_form_fields( $fields ){
    if( function_exists('is_product') && is_product() ){
        $fields_order=[ 'comment'=> null, 'author' => null, 'email' => null, 'cookies' => null ];
        $fields = array_replace_recursive($fields_order, $fields);
    }
    return $fields;
}


add_action('woocommerce_login_form_end', 'ep_set_social_login');
function ep_set_social_login(){
    echo "<hr>";
    echo do_shortcode('[yith_wc_social_login]');
}

/**
 * Validar teléfono a 10 dígitos
 */

add_filter( 'woocommerce_checkout_fields', 'ep_remove_default_phone_validation' );
function ep_remove_default_phone_validation( $fields ){
    unset( $fields['billing']['billing_phone']['validate'] );
	return $fields;
}
add_filter( 'woocommerce_billing_fields' , 'ep_custom_override_checkout_fields' );
function ep_custom_override_checkout_fields( $fields ) {
    $fields['billing_phone']['maxlength'] = 10;    
    return $fields;
}
add_action('woocommerce_checkout_process', 'ep_custom_validate_billing_phone');
function ep_custom_validate_billing_phone() {
    $is_correct = preg_match('/^[0-9]{10}$/', $_POST['billing_phone']);
    if ( $_POST['billing_phone'] && !$is_correct) {
        wc_add_notice( __( 'El número de teléfono tiene que ser <strong>de 10 dígitos</strong>.' ), 'error' );
    }
}


/**
 * Esconder campos para productos virtuales
 */

add_filter( 'woocommerce_checkout_fields' , 'ep_simplify_virtual_checkout' );
 
function ep_simplify_virtual_checkout( $fields ) {
   foreach( WC()->cart->get_cart() as $cart_item ) {
      if ( !$cart_item['data']->is_virtual() ) return $fields;   
   }
     
    unset($fields['billing']['billing_company']);
    unset($fields['billing']['billing_country']);
    unset($fields['billing']['billing_address_1']);
    unset($fields['billing']['billing_address_2']);
    unset($fields['billing']['billing_city']);
    unset($fields['billing']['billing_state']);
    unset($fields['billing']['billing_postcode']);
    unset($fields['billing']['billing_rfc']);
    unset($fields['billing']['billing_razon_social']);
    unset($fields['billing']['billing_send_billing']);
    //    add_filter( 'woocommerce_enable_order_notes_field', '__return_false' );

     return $fields;
}


add_action( 'woocommerce_email_before_order_table', 'ep_customer_email_barcode', 15, 4 );
function ep_customer_email_barcode( $order, $sent_to_admin, $plain_text, $email ) {
	$conekta_reference = get_post_meta( $order->get_id(), 'conekta-referencia', true );
	if ($conekta_reference != null){
		$barcode = ep_get_barcode_img($conekta_reference);
		echo '<div><strong>Código de barras para pago:</strong></div><div style="text-align: center;">'.$barcode.'</div>';
	}
}

add_action( 'woocommerce_thankyou_conektaoxxopay', 'ep_woocommerce_thankyou_barcode', 15, 1 );
function ep_woocommerce_thankyou_barcode($order_id){
	$conekta_reference = get_post_meta( $order_id, 'conekta-referencia', true );
	if ($conekta_reference != null){
		$barcode = ep_get_barcode_img($conekta_reference);
		echo '<div><strong>Código de barras para pago:</strong>'.$barcode.'</div>';
	}
}

function ep_get_barcode_img($reference){
	include_once 'barcode.php';
	$generator = new barcode_generator();
	$symbology = 'code-39';
	$options = array(
		'w' => '320',
		'h' => '100',
		'pb' => '30',
		'ts' => '14',
		'th' => '16',
	);
	$barcode = $generator->render_image($symbology, $reference, $options);
	$tmp = tempnam( sys_get_temp_dir(), 'barcode' );
	imagepng($barcode, $tmp);
	imagedestroy($image);
	$data = base64_encode(file_get_contents($tmp));
	@unlink($tmp);
	return '<img src="data:image/png;base64,'.$data.'" />';
}